package com.pagamento.client;

public class CartaoDTO {

    private Integer id;

    private String numero;

    private boolean ativo;

    private Integer clienteId;

    public CartaoDTO(Integer id, String numero, boolean ativo, Integer clienteId) {
        this.id = id;
        this.numero = numero;
        this.ativo = ativo;
        this.clienteId = clienteId;
    }

    public CartaoDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
