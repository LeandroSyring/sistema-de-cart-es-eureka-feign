package com.pagamento.client;

import com.pagamento.exception.CartaoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.http.HttpStatus;

public class CartaoClientErroDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {

        if(response.status() == 404){
            throw new CartaoNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
}
