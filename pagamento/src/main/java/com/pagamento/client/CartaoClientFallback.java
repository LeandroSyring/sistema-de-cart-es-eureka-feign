package com.pagamento.client;

import com.pagamento.exception.CartaoOfflineException;

import java.io.IOException;

public class CartaoClientFallback implements CartaoClient {

    private Exception cause;

    public CartaoClientFallback(Exception cause){
        this.cause = cause;
    }

    @Override
    public CartaoDTO buscarCartaoPorId(Integer id) {

        if(cause instanceof RuntimeException || cause instanceof IOException){
            throw new CartaoOfflineException();
        }
        throw (RuntimeException) cause;
    }
}