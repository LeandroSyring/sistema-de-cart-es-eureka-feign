package com.pagamento.client;

import com.pagamento.exception.ClienteOfflineException;

import java.io.IOException;

public class ClienteClientFallback implements ClienteClient{

    private Exception cause;

    public ClienteClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public ClienteDTO buscarClientePorID(Integer id) {

        if(cause instanceof RuntimeException || cause instanceof IOException){
            throw new ClienteOfflineException();
        }

        throw (RuntimeException) cause;
    }
}
