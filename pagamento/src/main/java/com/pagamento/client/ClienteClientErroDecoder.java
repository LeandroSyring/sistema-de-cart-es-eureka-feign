package com.pagamento.client;

import com.pagamento.exception.CartaoNotFoundException;
import com.pagamento.exception.ClienteNotFoundException;
import com.pagamento.exception.ClienteOfflineException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientErroDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {

        if(response.status() == 404){
            throw new ClienteNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
}
