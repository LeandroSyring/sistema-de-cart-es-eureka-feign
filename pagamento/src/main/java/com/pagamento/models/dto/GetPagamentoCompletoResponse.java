package com.pagamento.models.dto;

public class GetPagamentoCompletoResponse {

    private Integer idPagamento;

    private String descricaoPagamento;

    private Double valorPagamento;

    private Integer idCartao;

    private String numeroCartao;

    private Integer idCliente;

    private String nomeCliente;

    public GetPagamentoCompletoResponse(Integer idPagamento, String descricaoPagamento, Double valorPagamento, Integer idCartao, String numeroCartao, Integer idCliente, String nomeCliente) {
        this.idPagamento = idPagamento;
        this.descricaoPagamento = descricaoPagamento;
        this.valorPagamento = valorPagamento;
        this.idCartao = idCartao;
        this.numeroCartao = numeroCartao;
        this.idCliente = idCliente;
        this.nomeCliente = nomeCliente;
    }

    public GetPagamentoCompletoResponse() {
    }

    public Integer getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(Integer idPagamento) {
        this.idPagamento = idPagamento;
    }

    public String getDescricaoPagamento() {
        return descricaoPagamento;
    }

    public void setDescricaoPagamento(String descricaoPagamento) {
        this.descricaoPagamento = descricaoPagamento;
    }

    public Double getValorPagamento() {
        return valorPagamento;
    }

    public void setValorPagamento(Double valorPagamento) {
        this.valorPagamento = valorPagamento;
    }

    public Integer getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(Integer idCartao) {
        this.idCartao = idCartao;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
}
