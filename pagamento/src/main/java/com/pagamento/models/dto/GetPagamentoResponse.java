package com.pagamento.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class GetPagamentoResponse {

    private Integer id;

    private String descricao;

    private Double valor;

    private Integer cartao_id;

    public GetPagamentoResponse(Integer id, String descricao, Double valor, Integer cartao_id) {
        this.id = id;
        this.descricao = descricao;
        this.valor = valor;
        this.cartao_id = cartao_id;
    }

    public GetPagamentoResponse() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(Integer cartao_id) {
        this.cartao_id = cartao_id;
    }
}
