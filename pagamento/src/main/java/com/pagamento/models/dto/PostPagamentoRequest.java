package com.pagamento.models.dto;

public class PostPagamentoRequest {

    private String descricao;

    private Double valor;

    private Integer cartao_id;

    public PostPagamentoRequest(String descricao, Double valor, Integer cartao_id) {
        this.descricao = descricao;
        this.valor = valor;
        this.cartao_id = cartao_id;
    }

    public PostPagamentoRequest() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(Integer cartao_id) {
        this.cartao_id = cartao_id;
    }
}
