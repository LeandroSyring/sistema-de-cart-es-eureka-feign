package com.pagamento.models.mapper;

import com.pagamento.models.Pagamento;
import com.pagamento.models.dto.PostPagamentoRequest;

public class PagamentoRequestMapper {

    public static Pagamento fromPostPagamentoRequest(PostPagamentoRequest postPagamentoRequest){

        Pagamento pagamento = new Pagamento();
        pagamento.setCartaoId(postPagamentoRequest.getCartao_id());
        pagamento.setDescricao(postPagamentoRequest.getDescricao());
        pagamento.setValor(postPagamentoRequest.getValor());

        return pagamento;
    }
}
