package com.pagamento.models.mapper;

import com.pagamento.client.CartaoClient;
import com.pagamento.client.CartaoDTO;
import com.pagamento.client.ClienteClient;
import com.pagamento.client.ClienteDTO;
import com.pagamento.models.Pagamento;
import com.pagamento.models.dto.GetPagamentoCompletoResponse;
import com.pagamento.models.dto.GetPagamentoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PagamentoResponseMapper {

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private CartaoClient cartaoClient;

    public GetPagamentoCompletoResponse toGetPagamentoCompletoResponse(Pagamento pagamento){

        CartaoDTO cartaoDTO = cartaoClient.buscarCartaoPorId(pagamento.getCartaoId());

        ClienteDTO clienteDTO = clienteClient.buscarClientePorID(cartaoDTO.getClienteId());

        GetPagamentoCompletoResponse getPagamentoCompletoResponse = new GetPagamentoCompletoResponse();
        getPagamentoCompletoResponse.setIdPagamento(pagamento.getId());
        getPagamentoCompletoResponse.setDescricaoPagamento(pagamento.getDescricao());
        getPagamentoCompletoResponse.setValorPagamento(pagamento.getValor());
        getPagamentoCompletoResponse.setIdCartao(cartaoDTO.getId());
        getPagamentoCompletoResponse.setNumeroCartao(cartaoDTO.getNumero());
        getPagamentoCompletoResponse.setIdCliente(clienteDTO.getId());
        getPagamentoCompletoResponse.setNomeCliente(clienteDTO.getName());

        return getPagamentoCompletoResponse;
    }

    public static GetPagamentoResponse toGetPagamentoResponse(Pagamento pagamento){

        GetPagamentoResponse getPagamentoResponse = new GetPagamentoResponse();
        getPagamentoResponse.setId(pagamento.getId());
        getPagamentoResponse.setDescricao(pagamento.getDescricao());
        getPagamentoResponse.setValor(pagamento.getValor());
        getPagamentoResponse.setCartao_id(pagamento.getCartaoId());

        return getPagamentoResponse;
    }
}
