package com.pagamento.controllers;


import com.pagamento.models.Pagamento;
import com.pagamento.models.dto.GetPagamentoCompletoResponse;
import com.pagamento.models.dto.GetPagamentoResponse;
import com.pagamento.models.dto.PostPagamentoRequest;
import com.pagamento.models.mapper.PagamentoRequestMapper;
import com.pagamento.models.mapper.PagamentoResponseMapper;
import com.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @Autowired
    PagamentoResponseMapper pagamentoResponseMapper;

    @PostMapping
    public ResponseEntity<GetPagamentoResponse> inserirPagamento(@RequestBody PostPagamentoRequest postPagamentoRequest) {

        Pagamento pagamento = PagamentoRequestMapper.fromPostPagamentoRequest(postPagamentoRequest);
        Pagamento pagamentoSalvo = pagamentoService.inserirPagamento(pagamento);
        GetPagamentoResponse getPagamentoResponse = PagamentoResponseMapper.toGetPagamentoResponse(pagamentoSalvo);
        return ResponseEntity.status(201).body(getPagamentoResponse);
    }

    @GetMapping("/{idCartao}")
    public ResponseEntity<List<GetPagamentoResponse>> buscarPagamentoPorID(@PathVariable Integer idCartao){

        List<GetPagamentoResponse> getPagamentoResponsesList = new ArrayList<GetPagamentoResponse>();
        List<Pagamento> pagamentoList = pagamentoService.buscarPagamentoPorCartaoId(idCartao);
        for (Pagamento pag : pagamentoList) {
            GetPagamentoResponse getPagamentoResponse = PagamentoResponseMapper.toGetPagamentoResponse(pag);
            getPagamentoResponsesList.add(getPagamentoResponse);
        }
        return ResponseEntity.status(200).body(getPagamentoResponsesList);
    }

    @GetMapping("/completo/{idPagamento}")
    public ResponseEntity<GetPagamentoCompletoResponse> buscarPagamentoCompletoPorId(@PathVariable Integer idPagamento){

        Pagamento pagamento = pagamentoService.buscarPagamentoPorId(idPagamento);
        GetPagamentoCompletoResponse getPagamentoCompletoResponse = pagamentoResponseMapper.toGetPagamentoCompletoResponse(pagamento);
        return ResponseEntity.status(200).body(getPagamentoCompletoResponse);
    }
}
