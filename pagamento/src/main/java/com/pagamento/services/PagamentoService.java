package com.pagamento.services;

import com.pagamento.client.CartaoClient;
import com.pagamento.exception.CartaoNotFoundException;
import com.pagamento.exception.PagamentoNotFoundException;
import com.pagamento.models.Pagamento;
import com.pagamento.repositories.PagamentoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoClient cartaoClient;

    public Pagamento inserirPagamento(Pagamento pagamento){

        try{
            cartaoClient.buscarCartaoPorId(pagamento.getCartaoId());
            return pagamentoRepository.save(pagamento);
        }
        catch(FeignException.NotFound e){
            throw new CartaoNotFoundException();
        }
    }

    public List<Pagamento> buscarPagamentoPorCartaoId(Integer cartaoId){

        List<Pagamento> pagamentoList = pagamentoRepository.findAllByCartaoId(cartaoId);
        if(!pagamentoList.isEmpty()){
            return pagamentoList;
        }
        throw new PagamentoNotFoundException();
    }

    public Pagamento buscarPagamentoPorId(Integer id){

        Optional<Pagamento> pagamento = pagamentoRepository.findById(id);
        if(pagamento.isPresent()){
            return pagamento.get();
        }
        throw new PagamentoNotFoundException();
    }
}