package com.cartao.mapper;

import com.cartao.models.Cartao;
import com.cartao.models.dtos.CartaoRequestPostDto;

public class CartaoRequestMapper {

    public static Cartao fromCartaoRequestPostDto(CartaoRequestPostDto cartaoRequestPostDto){

        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoRequestPostDto.getNumero());
        cartao.setClienteId(cartaoRequestPostDto.getClienteId());

        return cartao;
    }
}
