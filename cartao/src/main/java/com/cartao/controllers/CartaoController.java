package com.cartao.controllers;

import com.cartao.mapper.CartaoRequestMapper;
import com.cartao.mapper.CartaoResponseMapper;
import com.cartao.models.Cartao;
import com.cartao.models.dtos.*;
import com.cartao.services.CartaoService;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RequestMapping("/cartao")
@RestController
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @PostMapping
    public ResponseEntity<CartaoResponsePostDto> inserirCartao(@RequestBody CartaoRequestPostDto cartaoRequestPostDto){

        Cartao cartao = CartaoRequestMapper.fromCartaoRequestPostDto(cartaoRequestPostDto);
        Cartao cartaoSalvo = cartaoService.inserirCartao(cartao);
        CartaoResponsePostDto cartaoResponsePostDto = CartaoResponseMapper.toCartaoResponsePostDto(cartaoSalvo);

        return ResponseEntity.status(201).body(cartaoResponsePostDto);
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<CartaoResponsePatchDto> atualizarStatus(@RequestBody CartaoRequestPatchDto cartaoRequestPatchDto, @PathVariable String numero){

        Cartao cartaoSalvo = cartaoService.atualizarStatus(numero, cartaoRequestPatchDto.isAtivo());
        CartaoResponsePatchDto cartaoResponsePatchDto = CartaoResponseMapper.toCartaoResponsePatchDto(cartaoSalvo);
        return ResponseEntity.status(201).body(cartaoResponsePatchDto);
    }

    @GetMapping("/{numero}")
    public ResponseEntity<CartaoResponseGetDto> buscarCartaoPorNumero(@PathVariable String numero){

        Cartao cartao = cartaoService.buscarPorNumero(numero).get();

        CartaoResponseGetDto cartaoResponseGetDto = CartaoResponseMapper.toCartaoResponseGetDto(cartao);

        return ResponseEntity.status(200).body(cartaoResponseGetDto);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<CartaoResponseGetDto> buscarCartaoPorId(@PathVariable Integer id){

        Cartao cartao = cartaoService.buscarCartaoPorId(id).get();

        CartaoResponseGetDto cartaoResponseGetDto = CartaoResponseMapper.toCartaoResponseGetDto(cartao);

        return ResponseEntity.status(200).body(cartaoResponseGetDto);
    }
}
