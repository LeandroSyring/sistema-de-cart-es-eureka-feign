package com.cartao.services;

import com.cartao.client.ClienteClient;
import com.cartao.client.ClienteDTO;
import com.cartao.exception.CartaoAlreadyExistsException;
import com.cartao.exception.CartaoNotFoundException;
import com.cartao.exception.ClienteNotFoundException;
import com.cartao.models.Cartao;
import com.cartao.repositories.CartaoRepository;
import feign.FeignException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteClient clienteClient;

    public Cartao inserirCartao(Cartao cartao){

        Optional<Cartao> cartaoExistenteOptional = cartaoRepository.findByNumero(cartao.getNumero());
        if(cartaoExistenteOptional.isPresent()){
            throw new CartaoAlreadyExistsException();
        }
        //try {
            ClienteDTO cliente = clienteClient.buscarClientePorID(cartao.getClienteId());
            //Regra de Negócio
            cartao.setAtivo(false);
            return cartaoRepository.save(cartao);
        //}
        //catch(FeignException.NotFound e){
            //throw new ClienteNotFoundException();
       // }
    }

    public Optional<Cartao> buscarPorNumero(String numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);

        if(cartaoOptional.isPresent()){
            return cartaoOptional;
        }
        throw new CartaoNotFoundException();
    }

    public Cartao atualizarStatus(String numero, boolean status) {

        Cartao cartao = this.buscarPorNumero(numero).get();
        cartao.setAtivo(status);
        return cartaoRepository.save(cartao);
    }

    public Optional<Cartao> buscarCartaoPorId(Integer id){

        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if(!cartaoOptional.isPresent()){
            throw new CartaoNotFoundException();
        }
        return cartaoOptional;
    }
}
