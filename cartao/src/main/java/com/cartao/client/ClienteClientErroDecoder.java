package com.cartao.client;

import com.cartao.exception.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientErroDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new ClienteNotFoundException();
        }
        else{
            return errorDecoder.decode(s, response);
        }
    }
}
