package com.cartao.client;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class ClienteDTO {

    private Integer id;

    private String name;

    public ClienteDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public ClienteDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
