package com.cartao.client;

import com.cartao.exception.ClienteNotFoundException;
import com.cartao.exception.ClienteOfflineException;

import java.io.IOException;

public class ClienteClientFallback implements ClienteClient {

    private Exception cause;

    ClienteClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public ClienteDTO buscarClientePorID(Integer id) {

        if(cause instanceof IOException || cause instanceof RuntimeException){
            throw new ClienteOfflineException();
            // throw new RuntimeException("Client serviço offline"); com esta linha, somente logamos o erro, mas a aplicação retorna 500
        }
        throw (RuntimeException) cause;
    }
}
