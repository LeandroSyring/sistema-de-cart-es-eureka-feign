package com.cartao.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cartão já existe com este número")
public class CartaoAlreadyExistsException extends RuntimeException {
}
